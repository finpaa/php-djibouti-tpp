<?php

namespace Finpaa\Djibouti;

class TPP
{
    private static $TOKEN;

    private static function getToken()
    {
        if (self::$TOKEN) {
            return self::$TOKEN;
        } else {
            self::getAuthorized();
            return self::getToken();
        }
    }

    private static function getAuthorized()
    {
        $body = array(
            "grant_type" => "client_credentials",
            "client_id" => env("DJIBOUTI_TPP_CLIENT_ID"),
            "client_secret" => env("DJIBOUTI_TPP_CLIENT_SECRET"),
            "scope" => "*"
        );

        $response = self::callServer('login', 'POST', [], $body);

        if (isset($response['access_token']))
            self::$TOKEN = 'Bearer ' . $response['access_token'];
        else
            self::$TOKEN = 'Unauthorized';
    }

    private static function callServer(string $path, $method = 'GET', $headers = [], $body = null): mixed
    {
        $curl = curl_init();
        $options = array(
            CURLOPT_URL => env('DJIBOUTI_TPP_HOST') . $path,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_POSTFIELDS => $body ? json_encode($body) : '',
            CURLOPT_HTTPHEADER => array(
                "Accept: application/json",
                "Content-Type: application/json",
                "X-Mock: " . request()->header('x-mock', 0)
            )
        );

        if (count($headers) > 0) {
            foreach ($headers as $key => $value) {
                $options[CURLOPT_HTTPHEADER][] = $key . ": " . $value;
            }
        }
        curl_setopt_array($curl, $options);
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            return "cURL Error #:" . $err;
        }
        return json_decode($response, true);
    }

    public static function getCountries()
    {
        $auth = [
            'Authorization' => self::getToken()
        ];
        return self::callServer('country', 'GET', $auth);
    }

    public static function getInstitutions($attributes)
    {
        // $path = 'institution?country_id=' . $country;
        $path = 'institution?' . http_build_query($attributes);

        $auth = [
            'Authorization' => self::getToken()
        ];
        return self::callServer($path, 'GET', $auth);
    }

    public static function getInstitution($code)
    {
        $path = 'institution?institution_code=' . $code;
        $auth = [
            'Authorization' => self::getToken()
        ];
        return self::callServer($path, 'GET', $auth);
    }

    public static function getAuthorizationMethods($institutionCode)
    {
        $path = 'processMethods/' . $institutionCode;
        $auth = [
            'Authorization' => self::getToken()
        ];
        return self::callServer($path, 'GET', $auth);
    }

    public static function getAccessToken($body)
    {
        $auth = [
            'Authorization' => self::getToken()
        ];
        return self::callServer('authorize', 'POST', $auth, $body);
    }

    public static function reattemptAccessToken($body)
    {
        $auth = [
            'Authorization' => self::getToken()
        ];
        return self::callServer('authorize/reattempt', 'POST', $auth, $body);
    }

    public static function resendAuth($body)
    {
        $auth = [
            'Authorization' => self::getToken()
        ];
        return self::callServer('authorize/resend', 'POST', $auth, $body);
    }

    public static function getAccounts($flow)
    {
        $body = [
            'flow' => $flow
        ];
        $auth = [
            'Authorization' => self::getToken()
        ];
        return self::callServer('accounts', 'POST', $auth, $body);
    }

    public static function getTransactions($flow, $accountId, $fromDate, $toDate)
    {
        $body = [
            'flow' => $flow,
            'accountId' => $accountId,
            'fromDate' => $fromDate,
            'toDate' => $toDate,
        ];

        $auth = [
            'Authorization' => self::getToken()
        ];
        return self::callServer('transactions', 'POST', $auth, $body);
    }

    public static function getPaymentTypes($institutionCode)
    {
        $path = 'paymentTypes/' . $institutionCode;
        $auth = [
            'Authorization' => self::getToken()
        ];
        return self::callServer($path, 'GET', $auth);
    }

    public static function initiatePayment($body)
    {
        $auth = [
            'Authorization' => self::getToken()
        ];
        return self::callServer('payment', 'POST', $auth, $body);
    }

    public static function paymentStatus($body)
    {
        $auth = [
            'Authorization' => self::getToken()
        ];
        return self::callServer('payment/status', 'POST', $auth, $body);
    }
}
